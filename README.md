# 画像認識システム

## django 起動コマンド

```
python3 manage.py runserver
```

## radis インストール
```
sudo apt install redis-server
```

## radis 起動コマンド

```
sudo service redis-server start
```

## celery 起動コマンド

```
celery -A mysite worker --pool solo -l info
```
