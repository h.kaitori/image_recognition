import numpy as np
from sklearn.cluster import KMeans
import cv2

from .custom import CustomConfig, CustomDataset

from .mrcnn import model as modellib
import os
import tensorflow as tf
from PIL import Image
from tensorflow.keras import models, layers, optimizers
from bs4 import BeautifulSoup
import requests
import io
from skimage import io as skimage_io
from urllib.parse import urlparse


# ディレクトリ
MODEL_DIR = "app/logs"
CUSTOM_DIR = "app/dataset"

DEVICE = "/gpu:0"

# モデル
MASKRCNN_MODEL_PATH = "app/h5_model/mask_rcnn_object.h5"
HUMAN_MODEL_PATH = 'app/h5_model/human_detect_model.hdf5'

# クラスター分割数
CLUSTER_NUM = 6

# 色定義
COLOR_HSV = [
    {
        'color': 'black',
        'range': [[360, 255, 60], [0, 0, 0]],
    },
    {
        'color': 'gray',
        'range': [[360, 19, 200], [0, 0, 61]],
    },
    {
        'color': 'white',
        'range': [[360, 19, 255], [0, 0, 201]],
    },
    {
        'color': 'red',
        'range': [[10, 255, 255], [0, 20, 50]],
    },
    {
        'color': 'skin',
        'range': [[35, 140, 255], [11, 20, 170]],
    },
    {
        'color': 'orange',
        'range': [[50, 255, 255], [11, 20, 50]],
    },
    {
        'color': 'yellow',
        'range': [[68, 255, 255], [51, 20, 50]],
    },
    {
        'color': 'green',
        'range': [[165, 255, 255], [69, 20, 50]],
    },
    {
        'color': 'blue',
        'range': [[250, 255, 255], [166, 20, 50]],
    },
    {
        'color': 'purple',
        'range': [[320, 255, 255], [251, 20, 50]],
    },
    {
        'color': 'red',
        'range': [[360, 255, 255], [321, 20, 50]],
    },
]


class InferenceConfig(CustomConfig):
    GPU_COUNT = 1
    IMAGES_PER_GPU = 1


# モデル読み込み
def setting_model():
    config = InferenceConfig()
    # config.display()
    dataset = CustomDataset()
    dataset.load_custom(CUSTOM_DIR, "val")
    dataset.prepare()

    with tf.device(DEVICE):
        model = modellib.MaskRCNN(
            mode="inference",
            model_dir=MODEL_DIR,
            config=config
        )

    model.load_weights(MASKRCNN_MODEL_PATH, by_name=True)
    return model


# マスク自動生成
def create_mask(img_src):
    # 商品画像
    # グレースケールに変換
    gray = cv2.cvtColor(img_src, cv2.COLOR_RGB2GRAY)

    # 2値化
    _, binary = cv2.threshold(gray, 230, 255, cv2.THRESH_BINARY_INV)

    # 輪郭を抽出
    contours, _ = cv2.findContours(
        binary, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE
    )

    mask = np.zeros_like(binary)

    # 輪郭内部 (透明化しない画素) を255で塗りつぶす
    cv2.drawContours(mask, contours, -1, color=255, thickness=-1)

    return mask


# 推論
def detect_color(image, model, clusters, label):
    # 画像読み込み
    img_src = skimage_io.imread(image)
    height = img_src.shape[0]
    width = img_src.shape[1]
    img_src = cv2.resize(img_src, (int(width*0.9), int(height*0.9)))
    no_create_mask = False

    # img_new = cv2.cvtColor(img_src, cv2.COLOR_BGR2RGB)
    img_new = img_src

    if label == 'product':
        mask = create_mask(img_src)
    elif label == 'person':
        # MaskRCNN推論
        results = model.detect([img_new], verbose=1)

        # マスク生成
        r = results[0]
        mask = r['masks']

        if mask.size:
            # モデル着用画像
            image_length = img_new.shape[0]
            mask = [[m[i][0] for m in list(mask)] for i in range(image_length)]
            mask = np.array(mask).astype(np.int8)
            # 回転
            mask = np.rot90(mask, 3)
            mask = np.fliplr(mask)
        else:
            mask = create_mask(img_src)
            no_create_mask = True

    # 画像とマスクを合成
    img_new = cv2.bitwise_and(img_new, img_new, mask=mask)

    # リサイズするパーセント
    scale_percent = 10

    # 50パーセント縮小
    width = int(img_new.shape[1] * scale_percent / 100)
    height = int(img_new.shape[0] * scale_percent / 100)
    dsize = (width, height)

    # 画像をリサイズ
    small_img = cv2.resize(img_new, dsize)

    # マスク以外を削除
    indToSelect = np.any(small_img != 0, keepdims=True, axis=2)[:, :, 0]
    forKmeans = small_img[indToSelect, :]

    # K平均法実行
    kmeans = KMeans(
        n_clusters=clusters,
        tol=1e-04,
    )
    kmeans.fit(forKmeans)

    # ヒストグラムで割合算出
    numLabels = np.arange(0, len(np.unique(kmeans.labels_)) + 1)
    (hist, _) = np.histogram(kmeans.labels_, bins=numLabels)
    hist = hist.astype("float")
    hist /= hist.sum()

    res = {
        'colors': kmeans.cluster_centers_.astype(int),
        'hist': hist,
        'no_create_mask': no_create_mask
    }

    return res


# RGBをHSVに変換
def rgb2hsv(r, g, b):
    maxcolor = max(r, g, b)
    mincolor = min(r, g, b)
    hsv = {
        'h': 0,
        's': 0,
        'v': maxcolor
    }

    if maxcolor != mincolor:
        if maxcolor == r:
            hsv['h'] = 60 * (g - b) / (maxcolor-mincolor)
        if maxcolor == g:
            hsv['h'] = 60 * (b - r) / (maxcolor-mincolor) + 120
        if maxcolor == b:
            hsv['h'] = 60 * (r - g) / (maxcolor-mincolor) + 240

        hsv['s'] = (maxcolor - mincolor) / maxcolor

    if hsv['h'] < 0:
        hsv['h'] = hsv['h'] + 360

    hsv['h'] = round(hsv['h'])
    hsv['s'] = round(hsv['s'] * 255)
    hsv['v'] = round((hsv['v'] / 255) * 255)
    return hsv


# 色割合算出
def create_color(model, predict_data):
    color_data = []

    for index, (url, label) in enumerate(predict_data.items()):
        parse = urlparse(url)
        filename = os.path.basename(parse.path)
        filename = filename.split('.')[0]
        # print(filename)
        # print(label)

        res = detect_color(url, model, CLUSTER_NUM, label)

        # 初期化
        for i in range(len(COLOR_HSV)):
            COLOR_HSV[i]['percent'] = 0

        for (percent, color) in zip(res['hist'], res['colors']):
            # print('color', color)

            r = color[0]
            g = color[1]
            b = color[2]

            hsv = rgb2hsv(r, g, b)
            h = hsv['h']
            s = hsv['s']
            v = hsv['v']
            # print('H:', h, ' | S:', s, ' | V:', v)
            # print('percent', str(round(percent * 100)) + '%')

            for i in range(len(COLOR_HSV)):
                mask = cv2.inRange(
                    np.array([h, s, v]),
                    np.array(COLOR_HSV[i]['range'][1]),
                    np.array(COLOR_HSV[i]['range'][0])
                )

                if mask[0] == 255 and mask[1] == 255 and mask[2] == 255:
                    # print(COLOR_HSV[i]['color'])
                    COLOR_HSV[i]['percent'] += percent

        found_color = {}
        for i in range(len(COLOR_HSV)):
            if COLOR_HSV[i]['percent'] > 0:
                percent = COLOR_HSV[i]['percent']
                color = COLOR_HSV[i]['color']
                found_color[color] = percent

        # print(found_color)

        # 肌の色を摘出
        if res['no_create_mask']:
            plus = 0
            for i in range(len(COLOR_HSV)):
                if COLOR_HSV[i]['color'] == 'skin' and COLOR_HSV[i]['percent'] > 0:
                    plus = found_color['skin'] / (len(found_color) - 1)
                if COLOR_HSV[i]['color'] == 'orange':
                    COLOR_HSV[i]['percent'] = found_color['orange'] - \
                        found_color['skin']

            if plus > 0:
                for key in found_color.keys():
                    for i in range(len(COLOR_HSV)):
                        if COLOR_HSV[i]['color'] == key:
                            COLOR_HSV[i]['percent'] += plus

        color_data.append(
            {
                'filename': filename,
                'label': label
            }
        )

        for i in range(len(COLOR_HSV)):
            output = round(COLOR_HSV[i]['percent'] * 100)
            color = COLOR_HSV[i]['color']

            if color in color_data[index]:
                color_data[index][color] += output
            else:
                color_data[index][color] = output

        color_data[index].pop('skin')

    return color_data


def build_model(in_space):
    nb_classes = 2
    model = models.Sequential()
    model.add(layers.Conv2D(64, (3, 3), padding='same', input_shape=in_space))
    model.add(layers.Activation('relu'))
    model.add(layers.MaxPooling2D(pool_size=(2, 2)))

    model.add(layers.Conv2D(128, (3, 3), padding='same'))
    model.add(layers.Activation('relu'))

    model.add(layers.Conv2D(256, (3, 3)))
    model.add(layers.Activation('relu'))
    model.add(layers.MaxPooling2D(pool_size=(2, 2)))

    model.add(layers.Conv2D(256, (3, 3)))
    model.add(layers.Activation('relu'))
    model.add(layers.MaxPooling2D(pool_size=(2, 2)))

    model.add(layers.Conv2D(64, (3, 3)))
    model.add(layers.Activation('relu'))
    model.add(layers.MaxPooling2D(pool_size=(2, 2)))

    model.add(layers.Flatten())
    model.add(layers.Dropout(0.5))

    model.add(layers.Dense(512))
    model.add(layers.Activation('relu'))
    model.add(layers.Dropout(0.5))

    model.add(layers.Dense(nb_classes))
    model.add(layers.Activation('sigmoid'))

    model.compile(loss='binary_crossentropy', optimizer=optimizers.Adam(
        learning_rate=0.000003), metrics=['accuracy'])
    return model


def pref(X, weight):
    model = build_model(X.shape[1:])
    model.load_weights(weight)
    pre = model.predict(X)
    print('全データ数は{}件です。'.format(len(X)))
    predict_person = []
    predict_product = []
    for idx, i in enumerate(pre):
        if float(i[0]) > 0.5:
            predict_person.append(idx)
        else:
            predict_product.append(idx)

    print('モデル着用画像は{}枚です。'.format(str(len(predict_person))))
    print('製品画像は{}枚です。'.format(str(len(predict_product))))
    return predict_person, predict_product


def human_predict(urls):
    image_size = 64

    X = []
    for url in urls:
        img = Image.open(io.BytesIO(requests.get(url).content))
        img = img.convert('L').convert('RGB').resize((image_size, image_size))
        data = np.asarray(img)
        X.append(data)
    X = np.array(X)

    predict_person, predict_product = pref(X, HUMAN_MODEL_PATH)
    predict_data = {}
    for i in predict_person:
        predict_data[urls[i]] = 'person'
    for i in predict_product:
        predict_data[urls[i]] = 'product'

    return predict_data


# オイラーにログイン
def login_euler():
    oyler_url = 'http://euler.aa3.netvolante.jp'
    login_url = oyler_url + '/new_oyler/Login'

    session = requests.session()
    response = session.get(login_url)
    bs = BeautifulSoup(response.text, 'html.parser')
    token_key = bs.find(
        attrs={'name': 'data[_Token][key]'}).get('value')
    token_fields = bs.find(
        attrs={'name': 'data[_Token][fields]'}).get('value')

    email = 'fermat_dbo'
    password = 'fermat486153'
    login_data = {
        'data[User][email]': email,
        'data[User][password]': password,
        'data[User][remember]': 1,
        'data[_Token][key]': token_key,
        'data[_Token][fields]': token_fields,
        'data[_Token][unlocked]': ''
    }

    response_cookie = response.cookies
    session.post(login_url, data=login_data, cookies=response_cookie)

    return session


# オイラーから画像URLを取得
def get_imgurl_from_euler(session, product_code):
    oyler_url = 'http://euler.aa3.netvolante.jp'
    imglist_url = oyler_url + '/new_oyler/image_list'

    response = session.get(imglist_url + '?item_name=' + product_code)
    bs = BeautifulSoup(response.text, 'html.parser')

    img_url = bs.select_one('img', class_='img-responsive')
    if img_url:
        img_url = oyler_url + img_url['src']
    else:
        img_url = ''

    return img_url

# TODO
# 色の範囲を管理画面で操作
