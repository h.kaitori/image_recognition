from django.contrib import admin
from .models import Product, Judge, Train

admin.site.register(Product)
admin.site.register(Judge)
admin.site.register(Train)
