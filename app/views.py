# django
from django.views.generic import View
from django.shortcuts import redirect, render, reverse
from django.http import HttpResponse, JsonResponse
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.contrib import messages
from django.db.models import Q

# inner
from .models import Judge, Product, Train
from .detect import login_euler, get_imgurl_from_euler, human_predict, setting_model, create_color

# celery
from celery import shared_task
from celery.result import AsyncResult

# system
import csv
import io
import pandas as pd
from functools import reduce
from operator import and_


HEADER_ERROR = 'ヘッダーが適切ではありません。修正後、再アップロードしてください'
EXTENSION_ERROR = '拡張子がCSVではありません。修正後、再アップロードしてください'
UNEXPECTED_NULL_ERROR = '入力必須項目が空欄になっています。アップロードしたファイルの内容を確認してください'
UNEXPECTED_DATA_ERROR = 'データの内容が不正です。ファイルの内容に文字化け等が含まれていないか確認してください'


# ページネーション
def paginate_queryset(request, queryset, count):
    paginator = Paginator(queryset, count)
    page = request.GET.get('page')
    try:
        page_obj = paginator.page(page)
    except PageNotAnInteger:
        page_obj = paginator.page(1)
    except EmptyPage:
        page_obj = paginator.page(paginator.num_pages)
    return page_obj


# nullチェック
def has_null(df, column):
    return df[column].isnull().any()


# 判定用
class IndexView(View):
    def get(self, request, *args, **kwargs):
        judge_data = Judge.objects.all().order_by('-id')
        total_count = len(judge_data)
        page_obj = paginate_queryset(request, judge_data, 10)

        return render(request, 'app/index.html', {
            'judge_data': page_obj.object_list,
            'page_obj': page_obj,
            'total_count': total_count
        })


# 検索判定用
class SearchJudgeView(View):
    def get(self, request, *args, **kwargs):
        judge_data = Judge.objects.all().order_by('-id')
        keyword = request.GET.get('keyword')

        if keyword:
            exclusion_list = [' ', '　']
            query_list = ''
            for word in keyword:
                if not word in exclusion_list:
                    query_list += word
            query = reduce(and_, [Q(name__icontains=q) for q in query_list])
            judge_data = judge_data.filter(query)

        total_count = len(judge_data)
        page_obj = paginate_queryset(request, judge_data, 10)

        return render(request, 'app/index.html', {
            'keyword': keyword,
            'judge_data': page_obj.object_list,
            'page_obj': page_obj,
            'total_count': total_count
        })


# 判定ファイルアップロード
class FileUploadJudgeView(View):
    def post(self, request, *args, **kwargs):
        file_data = request.FILES.get('file')
        filename = str(file_data)
        csv_check = False

        # CSVチェック
        if not filename.endswith('.csv'):
            messages.error(request, EXTENSION_ERROR)
            csv_check = True

        try:
            csvfile = io.TextIOWrapper(file_data, encoding='cp932')
            df = pd.read_csv(csvfile, keep_default_na=False, engine='python')
            if has_null(df, "product_code"):
                # product_codeカラムが存在しない
                messages.error(request, UNEXPECTED_NULL_ERROR)
                csv_check = True
        # ヘッダーが読み込めない
        except KeyError as e:
            messages.error(request, HEADER_ERROR)
            csv_check = True
        # 文字化け等によってCSVが正常に読み込めない
        except UnicodeDecodeError as e:
            messages.error(request, UNEXPECTED_DATA_ERROR)
            csv_check = True

        if not csv_check:
            judge_data = Judge()
            judge_data.name = str(file_data).split('.')[0]
            judge_data.filename = file_data
            judge_data.save()

            no_db_list = []
            for i in range(len(df)):
                product_code = df.iloc[i][0]
                # print(product_code)

                try:
                    product_data = Product.objects.get(
                        product_code=product_code
                    )
                    judge_data.product.add(product_data)
                    judge_data.save()
                except:
                    print(product_code, '存在しない')
                    no_db_list.append(product_code)

            judge_data.no_db = ','.join(no_db_list)
            judge_data.save()
            messages.success(request, "判定処理が完了しました。結果をご確認ください")

        return JsonResponse({
            'url': reverse('index')
        })


# 判定結果ダウンロード
class DownloadJudgeView(View):
    def get(self, request, *args, **kwargs):
        response = HttpResponse(content_type='text/csv; charset=Shift-JIS')
        judge_data = Judge.objects.get(id=self.kwargs['pk'])
        product_list = judge_data.product.all()

        header = ['product_code', 'black', 'gray', 'white',
                  'red', 'orange', 'yellow', 'green', 'blue', 'purple']
        response['Content-Disposition'] = 'attachment; filename="output.csv"'

        # responseに書き込み
        writer = csv.writer(response, quoting=csv.QUOTE_ALL)
        writer.writerow(header)

        for data in product_list:
            product_code = data.product_code
            black = str(data.black) + '%'
            gray = str(data.gray) + '%'
            white = str(data.white) + '%'
            red = str(data.red) + '%'
            orange = str(data.orange) + '%'
            yellow = str(data.yellow) + '%'
            green = str(data.green) + '%'
            blue = str(data.blue) + '%'
            purple = str(data.purple) + '%'

            row = []
            row += [product_code, black, gray,
                    white, red, orange, yellow, green, blue, purple]
            writer.writerow(row)

        return response


# DB存在しないダウンロード
class DownloadNoDBJudgeView(View):
    def get(self, request, *args, **kwargs):
        response = HttpResponse(content_type='text/csv; charset=Shift-JIS')
        judge_data = Judge.objects.get(id=self.kwargs['pk'])
        no_db = judge_data.no_db
        no_db_list = []
        if no_db:
            no_db_list = no_db.split(',')

        header = ['product_code']
        response['Content-Disposition'] = 'attachment; filename="no_db_output.csv"'

        # responseに書き込み
        writer = csv.writer(response, quoting=csv.QUOTE_ALL)
        writer.writerow(header)

        for code in no_db_list:
            writer.writerow([code])

        return response


# 学習用
class TrainView(View):
    def get(self, request, *args, **kwargs):
        train_data = Train.objects.all().order_by('-id')

        # ステータスがPENDING
        train_pending_data = train_data.filter(status='PENDING')

        for data in train_pending_data:
            task_id = data.task_id
            result = AsyncResult(task_id)
            train = Train.objects.get(task_id=task_id)
            train.status = result.status
            train.save()

        # オイラーに存在しない
        train_no_oyler_data = train_data.filter(no_oyler__isnull=False)

        for data in train_no_oyler_data:
            no_oyler_list = data.no_oyler.split(',')
            no_oyler_new_list = []

            # オイラーに存在していたら、上書き
            for product_code in no_oyler_list:
                product_data = Product.objects.filter(
                    product_code=product_code
                )
                if not product_data:
                    no_oyler_new_list.append(product_code)
            data.no_oyler = ",".join(no_oyler_new_list)
            data.save()

        train_data = Train.objects.all().order_by('-id')
        total_count = len(train_data)
        page_obj = paginate_queryset(request, train_data, 10)

        return render(request, 'app/train.html', {
            "train_data": page_obj.object_list,
            'page_obj': page_obj,
            "total_count": total_count,
        })


# 検索学習用
class SearchTrainView(View):
    def get(self, request, *args, **kwargs):
        train_data = Train.objects.all().order_by('-id')
        keyword = request.GET.get('keyword')

        if keyword:
            exclusion_list = [' ', '　']
            query_list = ''
            for word in keyword:
                if not word in exclusion_list:
                    query_list += word
            query = reduce(and_, [Q(name__icontains=q) for q in query_list])
            train_data = train_data.filter(query)

        total_count = len(train_data)
        page_obj = paginate_queryset(request, train_data, 10)

        return render(request, 'app/train.html', {
            'keyword': keyword,
            'train_data': page_obj.object_list,
            'page_obj': page_obj,
            'total_count': total_count
        })


@shared_task(bind=True)
def predict(self, train_id, product_code_list):
    print("処理中")

    # オイラー
    # 画像URL取得
    urls = []
    no_oyler_list = []
    color_data = []

    session = login_euler()
    for code in product_code_list:
        image_url = get_imgurl_from_euler(session, code)

        if image_url:
            urls.append(image_url)
        else:
            print('オイラー存在しない：', code)
            no_oyler_list.append(code)

    train_data = Train.objects.get(id=train_id)
    if no_oyler_list:
        train_data.no_oyler = ",".join(no_oyler_list)
        train_data.save()

    ## テスト用 ##
    # for code in product_code_list:
    #     urls.append(
    #         f'https://res.cloudinary.com/dss6ah7pd/image/upload/v1627951878/media/images/{code}.jpg')
    #############

    if urls:
        try:
            # モデル着用判定
            predict_data = human_predict(urls)

            # マスク生成
            model = setting_model()
            color_data = create_color(model, predict_data)
        except:
            print("処理失敗")
            raise Exception("マスクデータ生成失敗")

        for data in color_data:
            product_code = data["filename"]
            product_data, _ = Product.objects.get_or_create(
                product_code=product_code
            )
            product_data.label = data["label"]
            product_data.black = data["black"]
            product_data.gray = data["gray"]
            product_data.white = data["white"]
            product_data.red = data["red"]
            product_data.orange = data["orange"]
            product_data.yellow = data["yellow"]
            product_data.green = data["green"]
            product_data.blue = data["blue"]
            product_data.purple = data["purple"]
            product_data.is_exit = True
            product_data.save()

            train_data.product.add(product_data)
            train_data.save()
    else:
        print("処理失敗")
        raise Exception("オイラーからURLが取得できません")

    print("処理完了")

    return 'Done'


# 学習ファイルアップロード
class FileUploadTrainView(View):
    def post(self, request, *args, **kwargs):
        file_data = request.FILES.get('file')
        mode = self.kwargs['mode']

        filename = str(file_data)
        csv_check = False

        # CSVチェック
        if not filename.endswith('.csv'):
            messages.error(request, EXTENSION_ERROR)
            csv_check = True

        try:
            csvfile = io.TextIOWrapper(file_data, encoding='cp932')
            df = pd.read_csv(csvfile, keep_default_na=False, engine='python')
            if has_null(df, "product_code"):
                # product_codeカラムが存在しない
                messages.error(request, UNEXPECTED_NULL_ERROR)
                csv_check = True
        # ヘッダーが読み込めない
        except KeyError as e:
            messages.error(request, HEADER_ERROR)
            csv_check = True
        # 文字化け等によってCSVが正常に読み込めない
        except UnicodeDecodeError as e:
            messages.error(request, UNEXPECTED_DATA_ERROR)
            csv_check = True

        if not csv_check:
            product_code_list = []
            for i in range(len(df)):
                product_code = df.iloc[i][0]
                # すでに登録されている製品は除く
                if mode == 'diff':
                    product_data = Product.objects.filter(
                        product_code=product_code
                    )
                    if not product_data:
                        product_code_list.append(product_code)
                else:
                    product_code_list.append(product_code)

            if product_code_list:
                train_data = Train()
                train_data.name = str(file_data).split('.')[0]
                train_data.filename = file_data
                train_data.save()
                train_data.task_id = predict.delay(
                    train_data.id,
                    product_code_list
                )
                train_data.save()
                messages.success(request, "学習処理中です。しばらくお待ちください")

        return JsonResponse({
            'url': reverse('train')
        })


# オイラーに存在しないファイルダウンロード
class DownloadNoOlyerView(View):
    def get(self, request, *args, **kwargs):
        response = HttpResponse(content_type='text/csv; charset=Shift-JIS')
        train_data = Train.objects.get(id=self.kwargs['pk'])
        no_oyler = train_data.no_oyler
        product_code_list = []

        # オイラーに存在しない
        if no_oyler:
            product_code_list = no_oyler.split(',')

        header = ['product_code']
        response['Content-Disposition'] = 'attachment; filename="output_no_oyler.csv"'

        # responseに書き込み
        writer = csv.writer(response, quoting=csv.QUOTE_ALL)
        writer.writerow(header)

        for product_code in product_code_list:
            writer.writerow([product_code])

        return response


# 全製品コード色割合データをダウンロード
class DownloadProductCodeAllView(View):
    def get(self, request, *args, **kwargs):
        response = HttpResponse(content_type='text/csv; charset=Shift-JIS')

        header = ['product_code', 'black', 'gray', 'white',
                  'red', 'orange', 'yellow', 'green', 'blue', 'purple']
        response['Content-Disposition'] = 'attachment; filename="output_all.csv"'

        # responseに書き込み
        writer = csv.writer(response, quoting=csv.QUOTE_ALL)
        writer.writerow(header)

        product_data = Product.objects.all()

        for data in product_data:
            product_code = data.product_code
            black = str(data.black) + '%'
            gray = str(data.gray) + '%'
            white = str(data.white) + '%'
            red = str(data.red) + '%'
            orange = str(data.orange) + '%'
            yellow = str(data.yellow) + '%'
            green = str(data.green) + '%'
            blue = str(data.blue) + '%'
            purple = str(data.purple) + '%'

            row = []
            row += [product_code, black, gray,
                    white, red, orange, yellow, green, blue, purple]
            writer.writerow(row)

        return response


# オイラーに存在しない全データをダウンロード
class DownloadNoOylerAllView(View):
    def get(self, request, *args, **kwargs):
        response = HttpResponse(content_type='text/csv; charset=Shift-JIS')
        header = ['product_code']
        response['Content-Disposition'] = 'attachment; filename="output_no_oyler_all.csv"'

        # responseに書き込み
        writer = csv.writer(response, quoting=csv.QUOTE_ALL)
        writer.writerow(header)

        train_data = Train.objects.all()

        product_code_list = []
        for data in train_data:
            # オイラーに存在しない製品
            no_oyler = data.no_oyler
            if no_oyler:
                no_oyler = no_oyler.split(',')
                product_code_list.extend(no_oyler)

        for product_code in set(product_code_list):
            writer.writerow([product_code])

        return response
