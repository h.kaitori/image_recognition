from django.db import models
import os


# 製品
class Product(models.Model):
    product_code = models.CharField('製品コード', max_length=100)
    label = models.CharField('ラベル', max_length=100, null=True, blank=True)
    black = models.IntegerField('黒', default=0)
    gray = models.IntegerField('灰', default=0)
    white = models.IntegerField('白', default=0)
    red = models.IntegerField('赤', default=0)
    orange = models.IntegerField('橙', default=0)
    yellow = models.IntegerField('黄', default=0)
    green = models.IntegerField('緑', default=0)
    blue = models.IntegerField('青', default=0)
    purple = models.IntegerField('紫', default=0)

    def __str__(self):
        return self.product_code


# 判定
class Judge(models.Model):
    filename = models.FileField(
        upload_to='upload',
        verbose_name='判定アップロードファイル'
    )
    name = models.CharField("ファイル名", max_length=200)
    product = models.ManyToManyField(Product)
    no_db = models.CharField(
        "DB存在しない",
        max_length=100000,
        null=True,
        blank=True
    )
    created_at = models.DateTimeField("作成日", auto_now_add=True)

    def get_filename(self):
        return os.path.basename(self.filename.name)

    def __str__(self):
        return self.get_filename()


# 学習
class Train(models.Model):
    filename = models.FileField(
        upload_to='upload',
        verbose_name='学習アップロードファイル'
    )
    name = models.CharField("ファイル名", max_length=200)
    product = models.ManyToManyField(Product)
    task_id = models.CharField("タスクID", max_length=200, null=True, blank=True)
    status = models.CharField("ステータス", max_length=200, default="PENDING")
    no_oyler = models.CharField(
        'オイラー存在しない',
        max_length=100000,
        null=True,
        blank=True
    )
    created_at = models.DateTimeField("作成日", auto_now_add=True)

    def get_filename(self):
        return os.path.basename(self.filename.name)

    def __str__(self):
        return self.get_filename()
