from django.urls import path
from app import views

urlpatterns = [
    path('', views.IndexView.as_view(), name='index'),
    path('search_judge/', views.SearchJudgeView.as_view(), name='search_judge'),
    path('fileupload_judge/', views.FileUploadJudgeView.as_view(), name='fileupload_judge'),
    path('download_judge/<int:pk>/', views.DownloadJudgeView.as_view(), name='download_judge'),
    path('download_nodb_judge/<int:pk>/', views.DownloadNoDBJudgeView.as_view(), name='download_nodb_judge'),
    path('train/', views.TrainView.as_view(), name='train'),
    path('search_train/', views.SearchTrainView.as_view(), name='search_train'),
    path('fileupload_train/<str:mode>/', views.FileUploadTrainView.as_view(), name='fileupload_train'),
    path('download_no_oyler/<int:pk>/', views.DownloadNoOlyerView.as_view(), name='download_no_oyler'),
    path('download_product_code_all/', views.DownloadProductCodeAllView.as_view(), name='download_product_code_all'),
    path('download_no_oyler_all/', views.DownloadNoOylerAllView.as_view(), name='download_no_oyler_all'),
]
